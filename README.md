## Messenger API for Facebook Messenger Platfrom

[![npm](https://img.shields.io/npm/v/messenger-api.svg)](https://www.npmjs.com/package/messenger-api)
[![npm](https://img.shields.io/npm/dm/messenger-api.svg)](https://www.npmjs.com/package/messenger-api)
[![npm](https://img.shields.io/npm/dt/messenger-api.svg)](https://www.npmjs.com/package/messenger-api)


Messenger API is a NodeJS module that provides a simple wrapper interface to Facebook Messenger Platform API.

#### Current Features
* Supports FB Messenger API's new Profile API that is release in place for Messenger Threads.
* Uses FB MessengeObject modules for parsing of web hook events, postbacks, quick replies.

#### Upcoming Features
* Helper classes for various send templates

## Installation

```bash
npm install messenger-api --save
```

## License

MIT. Copyright (c) [Khang Toh](https://mogility.com).
