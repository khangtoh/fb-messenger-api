'use strict'
const crypto = require('crypto')
const request = require('request')
const sendRequest = (req, cb) => {
  
  request(req, (err, res, body) => {
    if (!cb) return
    if (err) return cb(err)
    if (body.error) return cb(body.error)
    cb(null, body)
  })
}

const baseURI = "https://graph.facebook.com"
const communityURI = "https://graph.facebook.com/community"
const DEFAULT_MEMBERS_FIELDS = ["first_name","last_name","picture","employee_number","link","name","name_format","locale","title","department","primary_address"]
const DEFAULT_GROUPS_FIELDS = ["id", "name", "picture", "updated_time"]


class CommunityAPI
{
  constructor(token, appId, appSecret, notificationType) 
  {
    this.token = token
    this.appId =  appId
    this.appSecret = appSecret 
    this.accessTokenProof = false
    this.notificationType = notificationType || 'REGULAR'
    this.req = {
      method: 'GET',
      uri: communityURI,
      qs: {
        access_token: token,
        fields: DEFAULT_MEMBERS_FIELDS.join()
      },
      json: true
    }
  }

  enableAccessTokenProof(enable)
  {
    this.accessTokenProof = enable
  }

  groups(cb)
  {
    this.req.uri = communityURI + '/groups'
    this.send(cb)
  }

  members(options, cb)
  {
    if (options instanceof Function) {
      cb = options;
      options = {};
    }
  
    this.req.uri = communityURI + '/members'
    if (options && options.limit)
    {
      this.req.uri = `${communityURI}/members?limit=${options.limit}`
    }
    this.send(cb)
  }

  admins(cb)
  {
    this.req.uri = communityURI + '/admins'
    this.send(cb)
  }

  group(groupId, cb)
  {
    this.req.uri = baseURI + `/${groupId}`
    this.req.qs.fields = DEFAULT_GROUPS_FIELDS.join()
    this.send(cb)
  }

  send(cb)
  {
    if (this.accessTokenProof)
    {
      const appsecretTime = Math.floor(Date.now() / 1000)-60;
      const appsecretProof = crypto
        .createHmac('sha256', this.appSecret)
        .update(this.token + '|' + appsecretTime)
        .digest('hex');

      this.req.qs.appsecret_proof = appsecretProof
      this.req.qs.appsecret_time = appsecretTime 
    }
    sendRequest(this.req, cb)
  }
}

module.exports = CommunityAPI
