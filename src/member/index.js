'use strict'
const crypto = require('crypto')
const request = require('request')
const sendRequest = (req, cb) => {
  
  request(req, (err, res, body) => {
    if (!cb) return
    if (err) return cb(err)
    if (body.error) return cb(body.error)
    cb(null, body)
  })
}

const messengerUserURI = "https://graph.facebook.com"
const DEFAULT_MEMBERS_FIELDS = ["email", "first_name","last_name","picture","employee_number","link","name","name_format","locale","title","department","primary_address"]

class MemberAPI
{
  constructor(token, appId, appSecret, notificationType) 
  {
    this.token = token
    this.appId =  appId
    this.appSecret = appSecret 
    this.accessTokenProof = false
    this.notificationType = notificationType || 'REGULAR'
    this.req = {
      method: 'GET',
      uri: messengerUserURI,
      qs: {
        access_token: token,
        fields: DEFAULT_MEMBERS_FIELDS.join()
      },
      json: {}
    }
  }

  enableAccessTokenProof(enable)
  {
    this.accessTokenProof = enable
  }

  get(userId, cb)
  {
    this.req.uri = messengerUserURI + `/${userId}`
    this.send(cb)
  }

  getWithGroups(userId, cb) {
    const limit=500
    this.req.uri = messengerUserURI + `/${userId}`
    this.req.qs.fields = [DEFAULT_MEMBERS_FIELDS].concat(`groups.limit(${limit})`).join()
    this.send(cb)
  }

  send(cb)
  {
    if (this.accessTokenProof)
    {
      const appsecretTime = Math.floor(Date.now() / 1000)-60;
      const appsecretProof = crypto
        .createHmac('sha256', this.appSecret)
        .update(this.token + '|' + appsecretTime)
        .digest('hex');

      this.req.qs.appsecret_proof = appsecretProof
      this.req.qs.appsecret_time = appsecretTime 
    }
    sendRequest(this.req, cb)
  }
}

module.exports = MemberAPI
