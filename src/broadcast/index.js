'use strict'
const request = require('request')
const crypto = require('crypto')

const sendRequest = (req, cb) => {
  request(req, (err, res, body) => {
    if (!cb) return
    if (err) return cb(err)
    if (body.error) return cb(body.error)
    cb(null, body)
  })
}
const messengerProfileURI = "https://graph.facebook.com"

class BroadcastRequest
{
  constructor(token, appId, appSecret, notificationType)
  {
    this.token = token
    this.appId =  appId
    this.appSecret = appSecret 
    this.accessTokenProof = false
  
    this.notificationType = notificationType || 'REGULAR'
    this.req = {
      method: 'GET',
      uri: messengerProfileURI,
      qs: {
        access_token: token
      },
      json: true,
      body: {}
    }
  }

  enableAccessTokenProof(enable)
  {
    this.accessTokenProof = enable
  }

  send(cb)
  {
    if (this.accessTokenProof)
    {
      const appsecretTime = Math.floor(Date.now() / 1000)-60;
      const appsecretProof = crypto
        .createHmac('sha256', this.appSecret)
        .update(this.token + '|' + appsecretTime)
        .digest('hex');
      
      this.req.qs.appsecret_proof = appsecretProof
      this.req.qs.appsecret_time = appsecretTime 
    }
    sendRequest(this.req, cb)
  }

  set(cb)
  {
    this.req.qs.fields=""
    this.send(cb)
  }

}

class BroadCastAPI 
{
  constructor(token, appId, appSecret, notificationType)
  {
    this.token = token
    this.appId =  appId
    this.appSecret = appSecret 
    this.accessTokenProof = false
    this.notificationType = notificationType || 'REGULAR'

    this.creative = new MessengeCreative(token, appId, appSecret, notificationType)
    this.broadcast = new BroadcastMessage(token, appId, appSecret, notificationType)
  }

  enableAccessTokenProof(enable)
  {
    this.accessTokenProof = enable
    this.creative.enableAccessTokenProof(enable)
    this.broadcast.enableAccessTokenProof(enable)
   
  }
}

class MessengeCreative extends BroadcastRequest 
{
  constructor(token, appId, appSecret, notificationType)
  {
    super(token, appId, appSecret, notificationType)
  }

  createText(text, cb)
  {
    let fb_text = text.replace("{{first_name}}","there!")
    fb_text = text.replace("{{last_time}}","")
    
    this.req.method="POST"
    this.req.uri = messengerProfileURI +'/me/message_creatives'
    this.req.body= {
      messages: [{
        'dynamic_text': {
          'text': text,
          'fallback_text': fb_text
        }
      }]
    }
    super.set(cb)
  }
}

class BroadcastMessage extends BroadcastRequest 
{
  constructor(token, appId, appSecret, notificationType)
  {
    super(token, appId, appSecret, notificationType)
   
  }

  send(messageCreativeId, tag, cb)
  {
    this.req.method="POST"
    this.req.uri = messengerProfileURI +'/me/broadcast_messages'
    this.req.body= {
      'message_creative_id': messageCreativeId,
      'notificationType': this.notificationType
    }
  
    if (typeof tag =="string")
    {

    }
    
    super.set(cb)
  }
}

module.exports = BroadCastAPI
