'use strict'
const request = require('request')
const crypto = require('crypto')
const sendRequest = (req, cb) => {
  request(req, (err, res, body) => {
    if (!cb) return
    if (err) return cb(err)
    if (body.error) return cb(body.error)
    cb(null, body)
  })
}

const messengerUserURI = "https://graph.facebook.com/v2.11"
const DEFAULT_USER_FIELDS = ["first_name","last_name","profile_pic","locale","timezone","gender"]

class UserAPI 
{
  constructor(token, appId, appSecret, notificationType) 
  {
    this.token = token
    this.appId = appId
    this.appSecret = appSecret
    this.accessTokenProof = false
    this.notificationType = notificationType || 'REGULAR'
    this.req = {
      method: 'GET',
      uri: messengerUserURI,
      qs: {
        access_token: token,
        fields: DEFAULT_USER_FIELDS.join()
      },
      json: {}
    }
  }
  
  enableAccessTokenProof(enable)
  {
    this.accessTokenProof = enable
  }

  send(cb)
  {
    if (this.accessTokenProof)
    {
      const appsecretTime = Math.floor(Date.now() / 1000)-60;
      const appsecretProof = crypto
        .createHmac('sha256', this.appSecret)
        .update(this.token + '|' + appsecretTime)
        .digest('hex');

      this.req.qs.appsecret_proof = appsecretProof
      this.req.qs.appsecret_time = appsecretTime 
    }
    sendRequest(this.req, cb)
  }

  get(userId, cb)
  {
    this.req.uri = messengerUserURI + `/${userId}`
    this.send(cb)
  }
}

module.exports = UserAPI
