'use strict'
const request = require('request')
const crypto = require('crypto')

const sendRequest = (req, cb) => {
  request(req, (err, res, body) => {
    if (!cb) return
    if (err) return cb(err)
    if (body.error) return cb(body.error)
    cb(null, body)
  })
}
const messengerProfileURI = "https://graph.facebook.com/me/messenger_profile"

class ProfileRequest
{
  constructor(token, appId, appSecret, notificationType)
  {
    this.token = token
    this.appId =  appId
    this.appSecret = appSecret 
    this.accessTokenProof = false
  
    this.notificationType = notificationType || 'REGULAR'
    this.req = {
      method: 'GET',
      uri: messengerProfileURI,
      qs: {
        access_token: token
      },
      json: true,
      body: {}
    }
  }

  enableAccessTokenProof(enable)
  {
    this.accessTokenProof = enable
  }

  send(cb)
  {
    if (this.accessTokenProof)
    {
      const appsecretTime = Math.floor(Date.now() / 1000)-60;
      const appsecretProof = crypto
        .createHmac('sha256', this.appSecret)
        .update(this.token + '|' + appsecretTime)
        .digest('hex');
      
      this.req.qs.appsecret_proof = appsecretProof
      this.req.qs.appsecret_time = appsecretTime 
    }
    sendRequest(this.req, cb)
  }

  set(cb)
  {
    this.req.qs.fields=""
    this.send(cb)
  }

}

class ProfileAPI 
{
  constructor(token, appId, appSecret, notificationType)
  {
    this.token = token
    this.appId =  appId
    this.appSecret = appSecret 
    this.accessTokenProof = false
    this.notificationType = notificationType || 'REGULAR'
    this.greeting = new GreetingText(token, appId, appSecret, notificationType)
    this.startButton = new StartButton(token, appId, appSecret, notificationType)
    this.domains = new DomainWhitelist(token,  appId, appSecret,notificationType)
    this.persistentMenu = new PersistentMenu(token, appId, appSecret, notificationType)
  }

  enableAccessTokenProof(enable)
  {
    this.accessTokenProof = enable
    this.greeting.enableAccessTokenProof(enable)
    this.startButton.enableAccessTokenProof(enable)
    this.domains.enableAccessTokenProof(enable)
    this.persistentMenu.enableAccessTokenProof(enable)
  }
}

class StartButton extends ProfileRequest 
{
  constructor(token, appId, appSecret, notificationType)
  {
    super(token, appId, appSecret, notificationType)
   
  }

  set(payload_string, cb) 
  { 
    
    this.req.method="POST"
    this.req.body.get_started={
      payload: payload_string
    }
    super.set(cb)
  }

  get(cb)
  {
    this.req.method="GET"
    this.req.qs.fields="get_started"
    send(this.req, cb)
  }

  delete(cb) {
    this.req.method="DELETE"
    this.req.body.fields = ["get_started"]

    this.send(cb)
  }
}

class GreetingText extends ProfileRequest 
{
  constructor(token, appId, appSecret, notificationType)
  {
    super(token, appId, appSecret, notificationType)
  }

  get(cb)
  {
    this.req.method="GET"
    this.req.qs.fields="greeting"
    
    this.send(cb)
  }

  set(greeting, cb)
  {
    this.req.method="POST"
    greeting.forEach(function(element) {
      if (element.text.length>160)
      {
        element.text = element.text.slice(0,160)
      }
    });
    this.req.body= {
      greeting: greeting
    }
    super.set(cb)
  }

  delete(cb)
  {
    this.req.method="DELETE"
    this.req.body.fields = ["greeting"]
    
    this.send(cb)(this.req, cb)
  }
}

class DomainWhitelist extends ProfileRequest 
{
  constructor(token, appId, appSecret, notificationType)
  {
    super(token, appId, appSecret, notificationType)
  }

  get(cb)
  {
    this.req.method="GET"
    this.req.qs.fields="whitelisted_domains"
    this.send(cb)
  }

  set(domains, cb)
  {
    this.req.method="POST"
    this.req.body.whitelisted_domains = domains
    super.set(cb)
  }

  delete(cb)
  {
    this.req.method="DELETE"
    this.req.body.fields = ["whitelisted_domains"]
    this.send(cb)
  }
}

class PersistentMenu extends ProfileRequest 
{
  constructor(token, appId, appSecret, notificationType)
  {
    super(token, appId, appSecret, notificationType)
  }

  get(cb)
  {
    this.req.method="GET"
    this.req.qs.fields="persistent_menu"
    this.send(cb)
  }

  set(menu,cb)
  {
    this.req.method="POST"
    this.req.body.persistent_menu = menu
    super.set(cb)
  }

  delete(cb)
  {
    this.req.method="DELETE"
    this.req.body.fields = ["persistent_menu"]
    this.send(cb)
  }
}

module.exports = ProfileAPI
