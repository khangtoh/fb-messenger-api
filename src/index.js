'use strict'
const request = require('request')
const crypto = require('crypto');
const qs = require('qs')

const ProfileAPI = require('./profile')
const SendAPI = require('./send')
const UserAPI = require('./user')
const MemberAPI = require('./member')
const CommunityAPI = require('./community')
const BroadCastAPI = require('./broadcast')

class FBMessenger 
{
  constructor(token, appId, appSecret, notificationType)
  {
    this.token = token
    this.appId = appId
    this.appSecret = appSecret
    this.notificationType = notificationType || 'REGULAR'
    this.profileAPI = new ProfileAPI(token, appId, appSecret,  notificationType)
    this.userAPI = new UserAPI(token,  appId, appSecret, notificationType)
    this.memberAPI = new MemberAPI(token,  appId, appSecret, notificationType)
    this.communityAPI = new CommunityAPI(token, appId, appSecret, notificationType)
    this.broadcastAPI = new BroadCastAPI(token, appId, appSecret, notificationType)
    this.accessTokenProof = false
  }

  enableAccessTokenProof(enable)
  {
    this.accessTokenProof = enable
    this.profileAPI.enableAccessTokenProof(enable)
    this.userAPI.enableAccessTokenProof(enable)
    this.memberAPI.enableAccessTokenProof(enable)
    this.broadcastAPI.enableAccessTokenProof(enable)
  }

  sendAction (id, action)
  {
    this.sendMessage(id, action)
  }

  sendTextMessage (id, text, notificationType, cb)
  {
    const messageData = {
      text: text
    }
    this.sendMessage(id, messageData, notificationType, cb)
  }

  sendImageMessage (id, imageURL, notificationType, cb)
  {
    const messageData = {
      'attachment': {
        'type': 'image',
        'payload': {
          'url': imageURL
        }
      }
    }
    this.sendMessage(id, messageData, notificationType, cb)
  }

  sendHScrollMessage (id, elements, notificationType, cb) 
  {
    const messageData = {
      'attachment': {
        'type': 'template',
        'payload': {
          'template_type': 'generic',
          'elements': elements
        }
      }
    }
    this.sendMessage(id, messageData, notificationType, cb)
  }

  sendButtonsMessage (id, text, buttons, notificationType, cb)
  {
    const messageData = {
      'attachment': {
        'type': 'template',
        'payload': {
          'template_type': 'button',
          'text': text,
          'buttons': buttons
        }
      }
    }
    this.sendMessage(id, messageData, notificationType, cb)
  }

  sendReceiptMessage (id, payload, notificationType, cb)
  {
    payload.template_type = 'receipt'
    const messageData = {
      'attachment': {
        'type': 'template',
        'payload': payload
      }
    }
    this.sendMessage(id, messageData, notificationType, cb)
  }

  sendQuickRepliesMessage (id, attachment, quickReplies, notificationType, cb) 
  {
    const attachmentType = (typeof attachment === 'string' ? 'text' : 'attachment')
    const attachmentObject = typeof attachment === 'string' ? attachment : {
      type: 'template',
      'payload': {
        'template_type': 'generic',
        'elements': attachment
      }
    }
    var messageData = {
      [attachmentType]: attachmentObject,
      'quick_replies': quickReplies
    }
    this.sendMessage(id, messageData, notificationType, cb)
  }

  sendFormDataMessage(formData, notificationType = this.notificationType, cb) {
    if (typeof notificationType === 'function') {
      cb = notificationType
      notificationType = this.notificationType
    }

    const req = {
      url: 'https://graph.facebook.com/me/messages',
      qs: {access_token: this.token},
      method: 'POST',
      formData: Object.assign({}, formData, {
        notification_type: notificationType
      })
    }

    if (this.accessTokenProof)
    {
      const appsecretTime = Math.floor(Date.now() / 1000)-60;
      const appsecretProof = crypto
        .createHmac('sha256', this.appSecret)
        .update(this.token + '|' + appsecretTime)
        .digest('hex');
        req.qs.appsecret_proof = appsecretProof
        req.qs.appsecret_time = appsecretTime
    }

    sendRequest(req, cb)
  }

  sendMessage (id, data, notificationType = this.notificationType, cb)
  {
    if (typeof notificationType === 'function') {
      cb = notificationType
      notificationType = this.notificationType
    }

    const json = {
      recipient: {
        id: id
      }
    }

    if (typeof data === 'string') {
      json.sender_action = data
    } else {
      json.message = data
      json.notification_type = notificationType
    }

    const req = {
      url: 'https://graph.facebook.com/me/messages',
      qs: {access_token: this.token},
      method: 'POST',
      json: json
    }

    if (this.accessTokenProof)
    {
      const appsecretTime = Math.floor(Date.now() / 1000)-60;
      const appsecretProof = crypto
        .createHmac('sha256', this.appSecret)
        .update(this.token + '|' + appsecretTime)
        .digest('hex');
        req.qs.appsecret_proof = appsecretProof
        req.qs.appsecret_time = appsecretTime
    }

    sendRequest(req, cb)
  }

  me(cb)
  {
    const req = {
      url: 'https://graph.facebook.com/me/',
      qs: {access_token: this.token},
      method: 'GET',
      json: true
    }

    if (this.accessTokenProof)
    {
      const appsecretTime = Math.floor(Date.now() / 1000)-60;
      const appsecretProof = crypto
        .createHmac('sha256', this.appSecret)
        .update(this.token + '|' + appsecretTime)
        .digest('hex');
        req.qs.appsecret_proof = appsecretProof
        req.qs.appsecret_time = appsecretTime
    }
    sendRequest(req, cb)
  }

  community(cb)
  {
    const req = {
      url: 'https://graph.facebook.com/community/',
      qs: {access_token: this.token},
      method: 'GET',
      json: true
    }

    if (this.accessTokenProof)
    {
      const appsecretTime = Math.floor(Date.now() / 1000)-60;
      const appsecretProof = crypto
        .createHmac('sha256', this.appSecret)
        .update(this.token + '|' + appsecretTime)
        .digest('hex');
        req.qs.appsecret_proof = appsecretProof
        req.qs.appsecret_time = appsecretTime
    }
    sendRequest(req, cb)
  }
}

const sendRequest = (req, cb) => {
  request(req, (err, res, body) => {
      if (!cb) return
    if (err) return cb(err)
    if (body.error) return cb(body.error)
    cb(null, res, body)
  })
}

module.exports = FBMessenger
